//
//  SymbolShape.swift
//  SetGame
//
//  Created by Hui Chih Wang on 2020/11/1.
//

import SwiftUI

struct Oval: Shape {
    func path(in rect: CGRect) -> Path {
        var p = Path()
        let center = CGPoint(x: rect.midX, y: rect.midY)
        
        let length = min(rect.width, rect.height) / 3
        let width = length * 0.9
        let height = length * 2
        
        let drawX = center.x - width / 2
        let drawY = center.y - height / 2
        
        let rectDraw = CGRect(x: drawX, y: drawY, width: width, height: height)
        
        p.addRoundedRect(in: rectDraw, cornerSize: CGSize(width: 30, height: 10))
        return p
    }
}

struct Squiggle: Shape {
    func path(in rect: CGRect) -> Path {
        var p = Path()
        let radius = min(rect.width, rect.height) / 2 * 0.5
        let center = CGPoint(x: rect.midX, y: rect.midY)
        
        p.addArc(center: center, radius: radius, startAngle: .degrees(0), endAngle: .degrees(360), clockwise: true)
        return p
    }
}

struct Diamond: Shape {
    func path(in rect: CGRect) -> Path {
        var p = Path()
        let center = CGPoint(x: rect.midX, y: rect.midY)
        
        let length = min(rect.width, rect.height)
        let width = length * 0.4
        let height = length * 0.7
        
        let vertices = [CGPoint(x: center.x + width / 2, y: center.y), CGPoint(x: center.x, y: center.y + height / 2), CGPoint(x: center.x - width / 2, y: center.y), CGPoint(x: center.x, y: center.y - height / 2), CGPoint(x: center.x + width / 2, y: center.y)]
        
        p.addLines(vertices)
        return p
    }
}


struct ShapeView: View {
    var body: some View {
        VStack(alignment: .center) {
            Oval()
                .fill(Color.orange)
                .frame(width: 100, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            Squiggle()
                .fill(Color.orange)
                .frame(width: 100, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            Diamond()
                .fill(Color.orange)
                .frame(width: 100, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            Rectangle()
                .fill(Color.orange)
                .frame(width: 100, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .clipShape(Oval())
            
        }
        
    }
}

struct SymbolShape_Previews: PreviewProvider {
    static var previews: some View {
        ShapeView()
    }
}
