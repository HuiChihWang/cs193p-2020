//
//  EmojiSetGame.swift
//  SetGame
//
//  Created by Hui Chih Wang on 2020/10/30.
//

import SwiftUI

class SetGameViewModel: ObservableObject {
    @Published private var setGame = SetGame()
    
    // MARK: - Access to model
    var cards: [Card<Symbol>] {
        setGame.cardsOnDeck
    }
    
    var matchPairs: Int {
        setGame.matchPairs
    }
    
    var isMatchOccur: Bool? {
        setGame.isMatch
    }

    // MARK: - Intents
    func startNewGame() {
        setGame = SetGame()
    }
    
    func choose(card: Card<Symbol>) {
        setGame.choose(card: card)
    }
    
    func dealMoreCards() {
        setGame.dealMoreCards()
    }
    

    
    
}
