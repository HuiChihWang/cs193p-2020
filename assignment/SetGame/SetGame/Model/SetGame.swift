//
//  SetGame.swift
//  SetGame
//
//  Created by Hui Chih Wang on 2020/10/30.
//

import Foundation

struct SetGame {
    private var cardSet = SetGame.createCardSet()
    private var cardsChosen = [Card<Symbol>]()

    private(set) var cardsOnDeck = [Card<Symbol>]()
    
    private static let numberOfCards = 12
    private static let numberOfFeatures = 4

    var matchPairs: Int = 0
    var isMatch: Bool?
    
    init() {
        refillCardsFromCardSet()
    }
    
    //MARK: - Intents
    mutating func choose(card: Card<Symbol>) {
        if let chooseIndex = cardsOnDeck.firstIndex(matching: card), !cardsOnDeck[chooseIndex].isMatch {
            
            // clear buffer
            if cardsChosen.count >= 3 {
                if let isMatch  = self.isMatch {
                    if !isMatch {
                        cardsChosen.forEach { card in
                            if let findIndex = cardsOnDeck.firstIndex(matching: card) {
                                cardsOnDeck[findIndex].isChosen = false
                            }
                        }
                    }
                    else {
                        print("card match and clear status")
                        removeMatchCards(matchCards: cardsChosen)
                        refillCardsFromCardSet()
                    }
                }
                
                cardsChosen.removeAll()
            }
            
            // card is not matched: change choose status
            cardsOnDeck[chooseIndex].isChosen = !cardsOnDeck[chooseIndex].isChosen
            
            // add card or remove card from buffer
            if let chosenIndex = cardsChosen.firstIndex(matching: cardsOnDeck[chooseIndex]) {
                // deselect card
                cardsChosen.remove(at: chosenIndex)
                print("deselect card, card buffer: \(cardsChosen.count)")

            }
            
            else {
                cardsChosen.append(cardsOnDeck[chooseIndex])
            }

            // card match process
            if cardsChosen.count == 3 {
                if SetGame.isCardSetMatch(cards: cardsChosen) {
                    matchPairs += 1
                    isMatch = true
                    print("Match Pairs: \(matchPairs)")
                }
                else {
                    isMatch = false
                }
            }
            else {
                isMatch = nil
            }
        }
    }
    
    mutating func dealMoreCards() {
        let fillCount = min(3, cardSet.count)
        for _ in 0..<fillCount{
            cardsOnDeck.append(cardSet.removeFirst())
        }
    }
    
    // MARK: - create a set of cards
    private static func createCardSet() -> [Card<Symbol>] {
        var cardSet = [Card<Symbol>]()
        
        for color in SymbolColor.allCases {
            for number in SymbolNumber.allCases {
                for shading in SymbolShading.allCases {
                    for shape in SymbolShape.allCases {
                        let symbol = Symbol(shape: shape, color: color, number: number, shading: shading)
                        cardSet.append(Card(content: symbol))
                    }
                }
            }
        }
        cardSet.shuffle()
        return cardSet
    }
    
    //MARK: - check whether there is a match
    private static func isCardSetMatch(cards: [Card<Symbol>]) -> Bool {
        
        var features = [[Int]](repeating: [Int](repeating: 0, count: 3), count: 4)
        
//        print("feature length: \(features.count)")
        cards.forEach { card in
            let symbol = card.content
            
            switch symbol.color {
            case .red:
                features[0][0] += 1
            case .green:
                features[0][1] += 1
            case .purple:
                features[0][2] += 1
            }
            
            switch symbol.shape {
            case .ovals:
                features[1][0] += 1
            case .squiggles:
                features[1][1] += 1
            case .diamonds:
                features[1][2] += 1
            }
            
            switch symbol.shading {
            case .solid:
                features[2][0] += 1
            case .striped:
                features[2][1] += 1
            case .outlined:
                features[2][2] += 1
            }
            
            switch symbol.number {
            case .one:
                features[3][0] += 1
            case .two:
                features[3][1] += 1
            case .three:
                features[3][2] += 1
            }
        }
        

        for feature in features {
            let find = feature.filter {$0 == 2}
            if !find.isEmpty {
                print("match fail")
                return false
            }
        }
    
        print("match success")
        return true
    }
    
    
    // MARK: - remove match cards
    mutating private func removeMatchCards(matchCards: [Card<Symbol>]) {
        for matchCard in matchCards {
            if let matchIndex = cardsOnDeck.firstIndex(matching: matchCard) {
                cardsOnDeck.remove(at: matchIndex)
            }
        }
    }
    
    // MARK: - refill the cards
    mutating private func refillCardsFromCardSet() {
        let cardNumber = cardsOnDeck.count
        let fillNumber = min(SetGame.numberOfCards - cardNumber, cardSet.count)
        
        if fillNumber > 0 {
            for _ in 0..<fillNumber {
                cardsOnDeck.append(cardSet.removeFirst())
            }
        }
    }
    

    
    
    
    
}
