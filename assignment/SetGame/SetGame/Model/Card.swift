//
//  Card.swift
//  SetGame
//
//  Created by Hui Chih Wang on 2020/10/30.
//

import Foundation


enum SymbolNumber: CaseIterable {
    case one
    case two
    case three
    
    var intNumber: Int {
        switch self {
        case .one:
            return 1
        case .two:
            return 2
        case .three:
            return 3
        }
    }
}

enum SymbolShape: CaseIterable {
    case ovals
    case squiggles
    case diamonds
}

enum SymbolColor: CaseIterable {
    case red
    case purple
    case green
}

enum SymbolShading: CaseIterable {
    case solid
    case striped
    case outlined
}


struct Symbol {
    var shape: SymbolShape
    var color: SymbolColor
    var number: SymbolNumber
    var shading: SymbolShading
}

struct Card<CardContent>: Identifiable {
    var isChosen: Bool = false
    var isMatch: Bool = false
    var content: CardContent
    var id = UUID()
}
