//
//  GridLayout.swift
//  Memorize
//
//  Created by CS193p Instructor.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import SwiftUI

struct GridLayout {
    private(set) var size: CGSize // layout size
    private(set) var rowCount: Int = 0
    private(set) var columnCount: Int = 0
    
    init(itemCount: Int, nearAspectRatio desiredAspectRatio: Double = 1, in size: CGSize) {
        self.size = size
        // if our size is zero width or height or the itemCount is not > 0
        // then we have no work to do (because our rowCount & columnCount will be zero)
        guard size.width != 0, size.height != 0, itemCount > 0 else { return }
        // find the bestLayout
        // i.e., one which results in cells whose aspectRatio
        // has the smallestVariance from desiredAspectRatio
        // not necessarily most optimal code to do this, but easy to follow (hopefully)
        let bestLayout: (rowCount: Int, columnCount: Int) = findBestLayout(sizeOfItems: self.size, numberOfItems: itemCount, targetRatio: desiredAspectRatio)

        rowCount = bestLayout.rowCount
        columnCount = bestLayout.columnCount
    }
    
    var itemSize: CGSize {
        if rowCount == 0 || columnCount == 0 {
            return CGSize.zero
        } else {
            return CGSize(
                width: size.width / CGFloat(columnCount),
                height: size.height / CGFloat(rowCount)
            )
        }
    }
    
    private func findBestLayout(sizeOfItems itemsSize: CGSize, numberOfItems itemCount: Int, targetRatio: Double) -> (rowCount: Int, columnCount: Int){
        let sizeAspectRatio = abs(Double(itemsSize.width/itemsSize.height))
        var bestLayout: (rowCount: Int, columnCount: Int) = (1, itemCount)
        var smallestVariance: Double?

        for rows in 1...itemCount {
            let columns = Int(ceil(Double(itemCount) / Double(rows)))
            let gridNumber = (rows) * columns
            
            if gridNumber - columns < itemCount {
                // layout depends on item ratio
                let itemAspectRatio = sizeAspectRatio / (Double(columns)/Double(rows))
                let variance = abs(itemAspectRatio - targetRatio)
                
                if smallestVariance == nil || variance < smallestVariance! {
                    smallestVariance = variance
                    bestLayout = (rowCount: rows, columnCount: columns)
                }
                
//                print("rows \(rows) columns \(columns)")
//                print("best Layout: \(bestLayout)")
//                print("item aspect ratio: \(itemAspectRatio)")
//                print("variance: \(variance)\n")
            }
            

            
        }
        
        return bestLayout
    }
    
    func location(ofItemAt index: Int) -> CGPoint {
        if rowCount == 0 || columnCount == 0 {
            return CGPoint.zero
        }
        else
        {
            let rowIndex = index / columnCount
            let columnIndex = index % columnCount
            
            return CGPoint(
                x: (CGFloat(columnIndex) + 0.5) * itemSize.width,
                y: (CGFloat(rowIndex) + 0.5) * itemSize.height
            )
        }
    }
}
