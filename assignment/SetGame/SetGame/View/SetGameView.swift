//
//  ContentView.swift
//  SetGame
//
//  Created by Hui Chih Wang on 2020/10/30.
//

import SwiftUI

struct SetGameView: View {
    @ObservedObject var setGameViewModel = SetGameViewModel()
    
    var body: some View {
        VStack {
            Text("Set Game")
                .font(.system(size: 40, weight: .bold))
                .padding(.top, 10)
            
                buildStatus()
                    .font(.system(size: 20))
                    .padding(.top, 10)
                    
                buildCards()
                
                Button("Deal more 3 cards") {
                    setGameViewModel.dealMoreCards()
                }
                .font(.system(size: 20))
                .padding(.vertical, 5)
            
            Button("New Game") {
                setGameViewModel.startNewGame()
            }
            .padding(.vertical, 5)
            .font(.system(size: 20))
        }
    }
    
    @ViewBuilder
    func buildStatus() -> some View {
        if let isMatch = setGameViewModel.isMatchOccur {
            Text(isMatch ? "Yeah! It's match" : "OOps! Match fail")
        }
        else {
            Text("Please select cards")
        }
    }
    
    
    func buildCards() -> some View {
        GeometryReader { geometry in
            Grid(items: setGameViewModel.cards) { card in
                return CardView(card: card)
                .padding(10)
                .onTapGesture {
                    withAnimation(nil){
                        setGameViewModel.choose(card: card)
                    }
                }
            }
            .padding()
        }
    }
}

extension AnyTransition {
    static var cardTransition: AnyTransition {
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        
        let randomX = CGFloat.random(in: -screenWidth..<screenWidth)
        let randomY = CGFloat.random(in: -screenHeight..<screenHeight)

        return AnyTransition.offset(x: randomX, y: randomY)
    }
}

struct EmojiSetGameView_Previews: PreviewProvider {
    static var previews: some View {
        SetGameView()
    }
    
    
    
}
