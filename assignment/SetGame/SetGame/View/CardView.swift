//
//  SwiftUIView.swift
//  SetGame
//
//  Created by Hui Chih Wang on 2020/10/30.
//

import SwiftUI




struct CardView: View {
    var card: Card<Symbol>
    
    var body: some View {
        GeometryReader { geometry in
            createCard(for: geometry.size)
        }
    }
    
    @State var defaultPos: CGPoint = CGPoint()
    private func startFlyAnimation() {
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        defaultPos.x = CGFloat.random(in: -width...width)
        defaultPos.y = CGFloat.random(in: -height...height)
        withAnimation(Animation.easeOut(duration: 2)) {
            defaultPos = CGPoint()
        }
    }
    
    @ViewBuilder
    private func createCard(for size: CGSize) -> some View {
        createBody()
        .offset(x: defaultPos.x, y: defaultPos.y)
        .scaleEffect(card.isChosen ? CGSize(width: 1.1, height: 1.1): CGSize(width: 1.0, height: 1.0))
        .onAppear{
            self.startFlyAnimation()
        }
    }
    
    private func createBody() -> some View {
        ZStack {
            RoundedRectangle(cornerRadius: 10)
                .fill(Color.white)
                .modifier(SymbolViewModifier(symbol: card.content))
            RoundedRectangle(cornerRadius: 10)
                .stroke(lineWidth: 3)
                .foregroundColor(card.isChosen ? .red : .blue)
                .shadow(radius: card.isChosen ? 10 : 0)
        }
    }
}

struct SymbolViewModifier: ViewModifier {
    var symbol: Symbol
    
    func body(content: Content) -> some View {
        ZStack {
            content
            HStack {
                ForEach(0..<symbol.number.intNumber) { index in
                    ZStack {
                        switch symbol.shape {
                        case .ovals:
                                Oval()
                                    .stroke(lineWidth: 3)
                                Oval()
                                    .modifier(SymbolFillModifier(fillMode: symbol.shading))
                        case .diamonds:
                                Diamond()
                                    .stroke(lineWidth: 3)
                                Diamond()
                                    .modifier(SymbolFillModifier(fillMode: symbol.shading))
                        case .squiggles:
                            Squiggle()
                                .stroke(lineWidth: 3)
                            Squiggle()
                                .modifier(SymbolFillModifier(fillMode: symbol.shading))
                        }
                    }
                    .modifier(SymbolColorModifier(symbolColor: symbol.color))
                }
            }
        }
    }
}


struct SymbolColorModifier: ViewModifier {
    private var color: Color
    
    init(symbolColor: SymbolColor) {
        switch symbolColor {
        case .red:
            color = .red
        case .green:
            color = .green
        case .purple:
            color = .purple
        }
    }
    
    func body(content: Content) -> some View {
        content
            .foregroundColor(color)
    }
}

struct SymbolFillModifier: ViewModifier {
    var fillMode: SymbolShading
    
    var opacity: Double {
        switch fillMode {
        case .outlined:
            return 0
        case .striped:
            return 0.5
        case .solid:
            return 1
        }
    }
    @ViewBuilder
    func body(content: Content) -> some View {
        content
            .opacity(opacity)
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        let card1 = Card(content: Symbol(shape: .ovals, color: .green, number: .two, shading: .striped))
        
        var card2 = card1
        card2.isChosen = true
        
        return VStack {
            CardView(card: card1)
            CardView(card: card2)
        }
        .frame(width: 100, height: 400, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
}
