//
//  ArrayExtension.swift
//  SetGame
//
//  Created by Hui Chih Wang on 2020/10/30.
//

import Foundation

extension Array where Element:Identifiable {
    
    // MARK: - find element by id
    func firstIndex(matching: Element) -> Int? {
        for index in 0..<self.count {
            if(self[index].id == matching.id) {
                return index
            }
        }
        return  nil
    }
}
