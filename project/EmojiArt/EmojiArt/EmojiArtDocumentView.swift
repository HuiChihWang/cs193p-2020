//
//  ContentView.swift
//  EmojiArt
//
//  Created by Hui Chih Wang on 2020/10/30.
//

import SwiftUI

struct EmojiArtDocumentView: View {
    @ObservedObject var document: EmojiArtDocument = EmojiArtDocument()
    
    
    var body: some View {
        VStack {
            ScrollView(.horizontal) {
                HStack {
                    ForEach(EmojiArtDocument.palette.map{String($0)}, id: \.self){ emoji in
                        Text(emoji)
                            .font(.system(size: defaultEmojiSize))
                            .onDrag{
                                return NSItemProvider(object: emoji as NSString)
                            }
                    }
                }
            }
            .padding(.horizontal)
            
            GeometryReader { geometry in
                ZStack{
                    Color.white
                        .overlay(
                            Group{
                                if let backgroundImage = self.document.backgroundImage {
                                    Image(uiImage: backgroundImage)
                                }
                            }
                        )
                        .edgesIgnoringSafeArea([.horizontal, .bottom])
                        //TODO: onDrop ?
                        .onDrop(of: ["public.image", "public.text"], isTargeted: nil) { providers, location in
                            var location = geometry.convert(location, from: .global)
                            let center = CGPoint(x: geometry.size.width / 2, y: geometry.size.height / 2)
                            location = CGPoint(x: location.x - center.x, y: location.y - center.y)
                            return self.drop(providers: providers, at: location)
                        }
                    ForEach(self.document.emojis) { emoji in
                        Text(emoji.text)
                            .font(self.font(for: emoji))
                            .position(self.position(for: emoji, in: geometry.size))
                        
                    }
                }

            }

        }
    }
    
    private func font(for emoji: EmojiArt.Emoji) -> Font {
        .system(size: emoji.fontize)
    }
    
    private func position(for emoji: EmojiArt.Emoji, in size: CGSize) -> CGPoint {
        let center = CGPoint(x: size.width / 2, y: size.height / 2)
        return CGPoint(x: emoji.location.x + center.x, y: emoji.location.y + center.y)
    }
    
    private func drop(providers: [NSItemProvider], at location: CGPoint) -> Bool {
        var found = providers.loadFirstObject(ofType: URL.self) { url in
            print("dropped \(url)")
            self.document.setBackgroundURL(url)
        }
        
        if !found {
            found = providers.loadObjects(ofType: String.self){ string in
                self.document.addEmoji(string, at: location, size: defaultEmojiSize)
            }
        }
        
        return found
    }
    
    private let defaultEmojiSize: CGFloat = 40.0
}

//struct ContentView_Previews: PreviewProvider {
//    static var previews: some View {
//        Group {
//            EmojiArtDocumentView()
//            EmojiArtDocumentView()
//        }
//    }
//}
