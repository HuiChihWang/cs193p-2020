//
//  EmojiMemoryGame.swift
//  Memorize
//
//  Created by Hui Chih Wang on 2020/10/2.
//

import SwiftUI

enum GameTheme: CaseIterable {
    case halloween
    case christmas
    case travel
    case sports
    case animals
    case foods
    
    var title: String {
        switch self {
        case .halloween:
            return "Halloween"
        case .christmas:
            return "Christmas"
        case .travel:
            return "Travel"
        case .sports:
            return "Sports"
        case .animals:
            return "Animals"
        case .foods:
            return "Foods"
        }
    }
    
    var emojiSet: [String] {
        switch self {
        case .halloween:
            return ["👻", "🎃", "🕷", "😈", "🍬"]
        case .animals:
            return ["🐶", "🐱", "🐰", "🐨", "🙈"]
        case .foods:
            return ["🍎", "🍑", "🥦", "🍌", "🍆"]
        case .sports:
            return ["🏀", "🎱", "🏈", "⚾️", "🏓"]
        case .travel:
            return ["🇹🇼", "🇺🇸", "🇵🇭", "🇦🇺", "🇮🇳"]
        case .christmas:
            return ["🎄", "🎅", "🍭", "🧦", "🕯"]
        }
    }
}

class EmojiMemoryGame: ObservableObject {
    @Published private var model: MemoryGame<String>
    
    @Published var gameTheme: GameTheme
    
    init() {
        let randomGameTheme = EmojiMemoryGame.getRandomGameTheme()
        gameTheme = randomGameTheme
        model = EmojiMemoryGame.createMemoryGame(gameTheme: randomGameTheme)
    }
    
    private static func getRandomGameTheme() -> GameTheme {
        return GameTheme.allCases.randomElement()!
    }
    
    private static func createMemoryGame(gameTheme: GameTheme) -> MemoryGame<String> {
        
        var pairsOfGame = Int.random(in: 2...5)
        pairsOfGame = 3
        
        return  MemoryGame<String>(numberOfPairsOfCards: pairsOfGame) { pairIndex in
            gameTheme.emojiSet[pairIndex]
        }
    }
    
    // MARK: - start play game
    func startNewGame() {
        gameTheme = EmojiMemoryGame.getRandomGameTheme()
        model = EmojiMemoryGame.createMemoryGame(gameTheme: gameTheme)
    }
    
    // MARK: - Intents
    func choose(card: MemoryGame<String>.Card) {
        objectWillChange.send()
        model.choose(card: card)
    }
    
    // MARK: - Access to model
    var cards: Array<MemoryGame<String>.Card> {
        model.cards
    }
    
    var score: Int {
        model.gameScore
    }
    
}

