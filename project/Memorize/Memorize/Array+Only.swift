//
//  Array+Only.swift
//  Memorize
//
//  Created by Hui Chih Wang on 2020/10/26.
//

import Foundation

extension Array {
    var only: Element? {
        count == 1 ? first : nil
    }
}
