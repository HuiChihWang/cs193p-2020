//
//  Pie.swift
//  Memorize
//
//  Created by Hui Chih Wang on 2020/10/26.
//

import SwiftUI

struct Pie: Shape {
    var startAngle: Angle
    var endAngle: Angle
    var clockwise: Bool = false
    
    // Animatable
    var animatableData: AnimatablePair<Double, Double> {
        get {
            AnimatablePair(startAngle.radians, endAngle.radians)
        }
        set {
            startAngle = Angle.radians(newValue.first)
            endAngle = Angle.radians(newValue.second)
        }
    }
    
    func path(in rect: CGRect) -> Path {
        var p = Path()
        
        let radius: CGFloat = min(rect.width, rect.height) / 2
        
        let center = CGPoint(x: rect.midX, y: rect.midY)
        
        let start = CGPoint(x: center.x + radius * cos(CGFloat(startAngle.radians)), y: center.y + radius * sin(CGFloat(startAngle.radians)))
        
        let end = CGPoint(x: center.x + radius * cos(CGFloat(endAngle.radians)), y: center.y + radius * sin(CGFloat(endAngle.radians)))
        
        p.move(to: center)
        p.addLine(to: start)
        p.addArc(center: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)
        p.addLine(to: end)
        
        return p
    }
}


