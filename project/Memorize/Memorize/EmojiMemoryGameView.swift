//
//  EmojiMemoryGameView.swift
//  Memorize
//
//  Created by Hui Chih Wang on 2020/9/30.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    @ObservedObject var viewModel: EmojiMemoryGame
    
    var body: some View {
        
        VStack {
            createTitle()
                .padding(.top, 10)
            createScoreBoard()
                .padding(.top, 10)
            createGrid()
            createNewGameButton()
        }
    }
    
    @ViewBuilder
    func createScoreBoard() -> some View {
            Text("Score: \(viewModel.score)")
                .font(.system(size: 25, weight: .heavy, design: .monospaced))
                .foregroundColor(Color(#colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)))
    }
    
    @ViewBuilder
    func createTitle() -> some View {
        Text(viewModel.gameTheme.title)
            .font(.system(size: 50, weight: .bold, design: .rounded))
    }
    
    @ViewBuilder
    func createNewGameButton() -> some View {
        
        ZStack {
            RoundedRectangle(cornerRadius: 10)
                .foregroundColor(Color(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)))
            
            Button(action: {
                withAnimation(.easeInOut) {
                    viewModel.startNewGame()
                }
            }, label: {
                    Text("New Game")
                        .foregroundColor(.white)
            })
        }
        .frame(width: 120, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
    
    @ViewBuilder
    func createGrid() -> some View {
        Grid(items: viewModel.cards){ card in
                CardView(card: card).onTapGesture{
                    withAnimation(.linear) {
                        viewModel.choose(card: card)
                    }
                }
                .padding(5)
        }
        .padding()
        .foregroundColor(Color.orange)
    }
}

struct CardView: View {
    var card: MemoryGame<String>.Card
    
    var body: some View {
        GeometryReader{ geometry in
            self.createBody(for: geometry.size)
        }
    }
    
    // MARK: - make animation on pie
    // if without this part, time consumption will not be continuous
    @State private var animatedBonusRemaining: Double = 0
    private func startBonusTimeAnimation() {
        animatedBonusRemaining = card.bonusRemaining
        withAnimation(.linear(duration: card.bonusTimeRemaining)) {
            animatedBonusRemaining = 0
        }
    }
    
    // MARK: - body creator
    @ViewBuilder
    private func createBody(for size: CGSize) -> some View {
        if !card.isMatched || card.isFaceUp {
            ZStack {
                Group {
                    if card.isConsumingBonusTime {
                        Pie(startAngle: Angle.degrees(-90), endAngle: Angle.degrees(-90 - animatedBonusRemaining * 360), clockwise: true)
                            // when card front appears
                            .onAppear{
                                self.startBonusTimeAnimation()
                        }
                    }
                    else {
                        Pie(startAngle: Angle.degrees(-90), endAngle: Angle.degrees(-90 - card.bonusRemaining*360), clockwise: true)
                    }
                }
                .opacity(0.4)
                .padding(5)

                
                Text(card.content)
                    .font(Font.system(size: fonSize(for: size)))
                    .rotationEffect(Angle.degrees(card.isMatched ? 360 : 0))
                    .animation(card.isMatched ?  Animation.linear.repeatForever(autoreverses: false) : .default)
            }
            .cardify(isFaceUp: card.isFaceUp)
            .transition(.scale)
        }
    }
    
    // MARK: - font size creator
    private let fontScaleFactor: CGFloat = 0.7
    private func fonSize(for size: CGSize) -> CGFloat {
        min(size.width, size.height) * fontScaleFactor
    }
    
}

struct EmojiMemoryGameView_Previews: PreviewProvider {
    static var previews: some View {
        let game = EmojiMemoryGame()
//        game.choose(card: game.cards[0])
        return EmojiMemoryGameView(viewModel: game)
    }
}
