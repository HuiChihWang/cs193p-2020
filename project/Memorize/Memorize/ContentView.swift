//
//  ContentView.swift
//  Memorize
//
//  Created by Hui Chih Wang on 2020/9/30.
//

import SwiftUI

struct EmojiMemoryGameView: View {
    var viewModel: EmojiMemoryGame
    var body: some View {
        let cardsView = HStack {
            ForEach(viewModel.cards) { card in
                CardView(card: card).onTapGesture(count: 1, perform: {
                    self.viewModel.choose(card: card)
                })
            }
        }
        .padding()
        .foregroundColor(Color.orange)
        
        if (viewModel.cards.count / 2 < 5) {
            cardsView.font(Font.largeTitle)
        }
        
    }
}

struct CardView: View {
    var card: MemoryGame<String>.Card
    
    var body: some View {
        ZStack {
            if card.isFaceUp {
                RoundedRectangle(cornerRadius: 10.0)
                    .fill(Color.white)
                    .aspectRatio(2/3, contentMode: .fit)
                RoundedRectangle(cornerRadius: 10.0)
                    .stroke(lineWidth: 3)
                    .aspectRatio(2/3, contentMode: .fit)
                Text(card.content)
            } else {
                RoundedRectangle(cornerRadius: 10.0)
                    .fill()
                    .aspectRatio(2/3, contentMode: .fit)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        EmojiMemoryGameView(viewModel: EmojiMemoryGame())
    }
}
